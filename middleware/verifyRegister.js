const mysql = require("mysql");
const bcrypt = require("bcrypt");

const mysqldb = mysql.createConnection({
  host: process.env.DATABASE_HOST,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE,
});

checkDuplicateEmail = (req, res, next) => {
  const { name, email, password } = req.body;
  mysqldb.query(
    "SELECT email FROM users WHERE email = ?",
    [email],
    async (error, results) => {
      if (error) {
        console.log(error);
      }
      if (results.length > 0) {
        res.status(400).send({
          message: "Failed! Email is already in use!",
        });
        return;
      }
      next();
    }
  );
};
const verifyRegister = {
  checkDuplicateEmail: checkDuplicateEmail,
};
module.exports = verifyRegister;
