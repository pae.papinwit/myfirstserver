const express = require("express");
const path = require("path");
const mysql = require("mysql");
const dotenv = require("dotenv");
const cookieSession = require("cookie-session");
const cors = require("cors");

const app = express();

const http = require("http");
const server = http.createServer(app);

const io = require("socket.io")(server, {
  cors: {
    origin: "*",
    //origin: "http://localhost:4200",
  },
});

var corsOptions = {
  origin: "*",
  //origin: "http://localhost:4200",
};
app.use(cors(corsOptions));
/*
io.use(function (socket, next) {
  if (socket.request.headers.cookie) return next();
  next(new Error("Authentication error"));
});*/

dotenv.config({ path: "./config.env" });

//SETUP MYSQL
const mysqldb = mysql.createConnection({
  host: process.env.DATABASE_HOST,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE,
});

//define component dir
const publicDirectory = path.join(__dirname, "public");
app.use(express.static(publicDirectory));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

/*
app.engine("html", require("ejs").renderFile);
app.set("view engine", "html");
*/

// APPLY COOKIE SESSION MIDDLEWARE
app.use(
  cookieSession({
    name: "session",
    keys: ["key1", "key2"],
    maxAge: 1000 * 60 * 60 * 2, // 2hr
  })
);

//MYSQL CONNECTION
mysqldb.connect((error) => {
  if (error) {
    console.log(error);
  } else {
    console.log("Database Connected...");
  }
});

//Express route
/*
app.use("/", require("./routes/auth.routes"));
app.use("/", require("./routes/user.routes"));
app.get("/", (req, res) => {
  res.json({ message: "Welcome to bezkoder application." });
});
*/

require("./routes/auth.routes")(app);
//require("./routes/user.routes")(app);

/*
app.use("/", require("./routes/pages"));
app.use("/", require("./routes/auth"));
app.use("/", (req, res) => {
  res.status(404).send("<h1>404 Page Not Found!</h1>");
});
*/
//Start Server

////////////////////////////////////////////////////////////////////////////////////////////
//Socket.IO

var users = [];
//const messages = [];
//var roomlist = [];
//io.listen(server);
io.on("connection", (socket) => {
  console.log("A user connected.");
  console.log(socket.id);
  socket.emit("Hello", "World!");
  ////
  socket.on("joined-user", (data) => {
    var user = { id: data.id, user: data.email, sid: data.sid };
    var usersIndex = users.findIndex((object) => object.id === user.id);
    if (usersIndex === -1) {
      users.push(user);
    }
    io.sockets.emit("send-online-users", users);
  });
  ////
  let previousId;
  const safeJoin = (currentId) => {
    console.log("safejoin");
    socket.leave(previousId);
    socket.join(currentId);
    previousId = currentId;
  };

  socket.on("join", (roomName) => {
    console.log("join: " + roomName);
    safeJoin(roomName);
  });

  socket.on("send_msg", (data) => {
    console.log("snd_msg: ");
    console.log(data);
    io.sockets.to(data.rid).emit("send_msg", data);

    /*
    var usersIndex = messages.findIndex((object) => object.rid === data.rid);
    if (usersIndex === -1) {
      messages.push(data);
      io.sockets.emit("rec_send_msg", messages[messages.length - 1].msg);
    } else {
      io.sockets.emit("rec_send_msg", messages[usersIndex].msg);
    }*/
  });
  /*
  socket.on("new_message", (data) => {
    io.sockets.emit("receive_message", {
      message: data.message,
      username: socket.username,
    });
  });

  socket.on("request-online-user", (request) => {
    io.sockets.emit("send-online-users", users);
    console.log("Request online users =>");
    console.log(users);
  });
*/
  socket.on("user-disconnected", (data) => {
    console.log("User " + data + " Logged out");
    var usersIndex = users.findIndex((obj) => {
      return obj.user === data;
    });
    if (usersIndex !== -1) {
      users.splice(usersIndex, 1);
    }
    console.log(users);
    io.sockets.emit("send-online-users", users);
  });
});

server.listen(5000, () => {
  console.log("Server started on port 5000");
});
