const config = require("../config/auth.config");
var jwt = require("jsonwebtoken");
var bcrypt = require("bcrypt");
const mysql = require("mysql");

//SETUP MYSQL
const mysqldb = mysql.createConnection({
  host: process.env.DATABASE_HOST,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE,
});

exports.register = (req, res) => {
  // Save User to Database
  const { name, email, password } = req.body;
  console.log(req.body);
  mysqldb.query(
    "SELECT email FROM users WHERE email = ?",
    [email],
    async (error, results) => {
      if (error) {
        console.log(error);
      }
      if (results.length > 0) {
        return res.send({ message: "This email is already in use." });
      }
      let hashPass = await bcrypt.hash(password, 8);
      console.log(hashPass);
      mysqldb.query(
        "INSERT INTO users SET?",
        {
          name: name,
          email: email,
          password: hashPass,
        },
        (error, results) => {
          if (error) {
            console.log(error);
          } else {
            res.send({ message: "User was registered successfully!" });
          }
        }
      );
    }
  );
};

exports.login = (req, res) => {
  const { email, password } = req.body;
  mysqldb.query(
    "SELECT * FROM users WHERE email = ?",
    [email],
    async (error, results) => {
      if (error) {
        console.log(error);
      }
      if (results.length == 0) {
        return res.status(404).send({ message: "User Not found." });
      }

      var passwordIsValid = bcrypt.compare(password, results[0].password);
      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "Invalid Password!",
        });
      }
      var token = jwt.sign({ id: results[0].id }, config.secret, {
        expiresIn: 86400, // 24 hours
      });

      res.status(200).send({
        id: results[0].id,
        username: results[0].username,
        email: results[0].email,
        accessToken: token,
      });
    }
  );
};
